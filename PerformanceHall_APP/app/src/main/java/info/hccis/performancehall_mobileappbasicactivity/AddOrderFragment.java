package info.hccis.performancehall_mobileappbasicactivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderBO;
import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderViewModel;
import info.hccis.performancehall_mobileappbasicactivity.databinding.FragmentAddOrderBinding;

public class AddOrderFragment extends Fragment {

    public static final String KEY = "info.hccis.performancehall.ORDER";
    private FragmentAddOrderBinding binding;
    TicketOrderBO ticketOrderBO;
    TicketOrderViewModel ticketOrderViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddOrderFragment BJM","onCreate triggered");
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddOrderFragment BJM","onCreateView triggered");
        binding = FragmentAddOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }



    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddOrderFragment BJM","onViewCreated triggered");

        ticketOrderViewModel = new ViewModelProvider(getActivity()).get(TicketOrderViewModel.class);

        binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddOrderFragment BJM", "Calculate was clicked");

                try {
                    calculate();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, ticketOrderBO);





                    NavHostFragment.findNavController(AddOrderFragment.this)
                            .navigate(R.id.action_AddOrderFragment_to_ViewOrdersFragment, bundle);
                }catch(Exception e){
                    Log.d("AddOrderFragment BJM", "Error calculating: "+e.getMessage());
                }
            }
        });

    }

    /**
     * calculate the ticket cost based on the controls on the view.
     * @since 20220118
     * @author CIS2250
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     */
    public void calculate() throws Exception{

        Log.d("BJM-MainActivity", "HollPass Number entered =" + binding.editTextHollpassNumber.getText().toString());
        Log.d("BJM-MainActivity", "Number of tickets = " + binding.editTextNumberOfTickets.getText().toString());
        Log.d("BJM-MainActivity", "Calculate button was clicked.");

        int hollPassNumber = 0;
        try {
            hollPassNumber = Integer.parseInt(binding.editTextHollpassNumber.getText().toString());
        }catch(Exception e){
            hollPassNumber = 0;
        }
        boolean validHollPassNumber;
        int numberOfTickets;
        try {
            numberOfTickets = Integer.parseInt(binding.editTextNumberOfTickets.getText().toString());
        }catch(Exception e){
            numberOfTickets = 0;
        }
        ticketOrderBO = new TicketOrderBO(hollPassNumber, numberOfTickets);

        try {
            if (!ticketOrderBO.validateNumberOfTickets()) {
                throw new Exception("Invalid Number of tickets");
            }
            double cost = ticketOrderBO.calculateTicketPrice();

            ticketOrderViewModel.getTicketOrders().add(ticketOrderBO);

            //Now that I have the cost, want to set the value on the textview.
            Locale locale = new Locale("en", "CA");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            decimalFormat.setDecimalFormatSymbols(dfs);
            String formattedCost = decimalFormat.format(cost);
            binding.textViewCost.setText(formattedCost);

        }catch(NumberFormatException nfe){
            binding.editTextNumberOfTickets.setText("");
            binding.textViewCost.setText("Invalid number of tickets");
            throw nfe;
        }catch(Exception e){
            binding.editTextNumberOfTickets.setText("");
            binding.textViewCost.setText("Maximum number of tickets is "+TicketOrderBO.MAX_TICKETS);
            throw e;
        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
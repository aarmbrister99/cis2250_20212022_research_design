package info.hccis.performancehall_mobileappbasicactivity.bo;


import java.io.Serializable;
import java.time.LocalTime;

/**
 * Business logic class for ticket purchaes
 *
 * @author bjmaclean
 * @since 20220105
 */
public class TicketOrderBO implements Serializable {

    public static final double MAX_TICKETS =100;
    public static final double COST_TICKET =10;
    public static final double DISCOUNT_HOLLPASS = 0.1;
    public static final double DISCOUNT_VOLUME_10 = 0.1;
    public static final double DISCOUNT_VOLUME_20 = 0.15;

    private int hollPassNumber;
    private boolean hasHollPass;
    private int numberOfTickets;
    private double cost;

    public TicketOrderBO(int hollPassNumber, int numberOfTickets) {

        this.hollPassNumber = hollPassNumber;
        this.numberOfTickets = numberOfTickets;
        this.setHasHollPass(validateHollPassNumber());
    }

    /**
     * Calculate the cost for a ticket order
     *
     * @return cost
     * @since 20220105
     * @author cis2250
     */
    public double calculateTicketPrice() {
        double discount = 0;
        if(numberOfTickets >=20){
            discount += DISCOUNT_VOLUME_20;
        }else if(numberOfTickets >=10){
            discount += DISCOUNT_VOLUME_10;
        }

        if(hasHollPass){
            discount += DISCOUNT_HOLLPASS;
        }

        cost = numberOfTickets * COST_TICKET * (1-discount);

        return cost;
    }

    /**
     * Verify if a HollPassNumber if correct
     * Rules:  Length is 5 and a multiple of 13
     *
     * @return valid result
     * @since 20220105
     * @author cis2250
     */
    public boolean validateHollPassNumber() {

        boolean valid = false;

        if(hollPassNumber >= 10000 && hollPassNumber < 100000){
            if(hollPassNumber % 13 == 0){
                valid=true;
            }
        }

        return valid;
    }

    /**
     * Return if a valid number of tickets or not
     * @return true if valid
     * @since 20220117
     * @author BJM
     */
    public boolean validateNumberOfTickets(){
        if(numberOfTickets > 0 && numberOfTickets <= MAX_TICKETS){
            return true;
        }else{
            return false;
        }
    }

    public int getHollPassNumber() {
        return hollPassNumber;
    }

    public void setHollPassNumber(int hollPassNumber) {
        this.hollPassNumber = hollPassNumber;
    }

    public boolean isHasHollPass() {
        return hasHollPass;
    }

    public void setHasHollPass(boolean hasHollPass) {
        this.hasHollPass = hasHollPass;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Ticket Order" + System.lineSeparator()
                +"HollPass Number=" + hollPassNumber +
                ", has HollPass=" + hasHollPass +
                ", Number of Tickets=" + numberOfTickets+System.lineSeparator()
                +"Order cost: $"+calculateTicketPrice();
    }
}
